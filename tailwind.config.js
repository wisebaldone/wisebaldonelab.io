/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{html,ts}",
  ],
  theme: {
    extend: {},
    gridTemplateColumns: {
      'core': 'minmax(1rem, 1fr) minmax(1rem, 1fr) minmax(1rem, 1fr) minmax(auto, 1148px) minmax(1rem, 1fr) minmax(1rem, 1fr) minmax(1rem, 1fr)',
      'core-lg': '1fr 1fr 2fr 1184px 2fr 1fr 1fr'
    },
    fontFamily: {
      'opensans' : ['Open Sans', 'sans-serif']
    },
    colors: {
      'sharde-blue': '#456ADE'
    }
  },
  plugins: [],
}
