import * as THREE from "three";

export class Hexagon {

  private trail: any;

  private test!: THREE.Mesh;

  constructor(
    private scale: number,
    private filled: boolean,
    private fill: number,
    private trailLength: number,
    private velocityX: number, private velocityY: number,
    private x: number, private y: number,
    private pitch: number
  ) {
    let innerRadius = filled ? 0: (8 * scale);
    let geometry = new THREE.RingGeometry( innerRadius, 10 * scale, 6 );
    let material = new THREE.MeshBasicMaterial({
        color: this.fill,
        side: THREE.DoubleSide
      });

    this.test = new THREE.Mesh(geometry, material);
    this.test.position.set(x, y, 0);
  }

  public animate(canvas: HTMLCanvasElement)
  {
    let translatedX = this.test.position.x + (canvas.clientWidth / 2);
    let translatedY = this.test.position.y + (canvas.clientHeight / 2);

    translatedX = translatedX < 0 ? translatedX + canvas.clientWidth : translatedX;
    translatedY = translatedY < 0 ? translatedY + canvas.clientHeight : translatedY;

    translatedX += this.velocityX;
    translatedY += this.velocityY;
    translatedX = translatedX % canvas.clientWidth;
    translatedY = translatedY % canvas.clientHeight;

    this.test.position.x = (translatedX - (canvas.clientWidth / 2));
    this.test.position.y = (translatedY - (canvas.clientHeight / 2));

    this.test.rotateZ(this.pitch);
  }

  public addToScene(scene: THREE.Scene) {
    scene.add(this.test);
  }

}
