import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import * as THREE from "three";
import {Hexagon} from "./hexagon";

@Component({
  selector: 'app-wallpaper',
  templateUrl: './wallpaper.component.html',
  styleUrls: ['./wallpaper.component.scss']
})
export class WallpaperComponent implements OnInit, AfterViewInit {

  @ViewChild('wallpaper')
  private canvasRef!: ElementRef;

  private camera!: THREE.OrthographicCamera;
  private renderer!: THREE.WebGLRenderer;
  private scene!: THREE.Scene;

  private loader = new THREE.TextureLoader();
  private geometry = new THREE.RingGeometry( 0, 1, 6 );
  private material = new THREE.MeshBasicMaterial({
    color: 0x877CFF,
    side: THREE.DoubleSide
  });

  private hex: THREE.Mesh = new THREE.Mesh(this.geometry, this.material);

  private hexColours: Array<number> = [0x877CFF, 0xD0EEE5, 0x88BFFF, 0x249ACC];
  private hexs: Array<Hexagon> = [];

  private get canvas(): HTMLCanvasElement {
    return this.canvasRef.nativeElement;
  }

  constructor() { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.createScene();
    this.startRenderingLoop();
  }

  private createScene() {
    this.scene = new THREE.Scene();

    // this.hexs.push(new Hexagon(1, true, 0x877CFF, 0, 1, 1, -200, -599, -0.01));
    // this.hexs.push(new Hexagon(0.5, true, 0xD0EEE5, 0, -1, -2, 18, -689, -0.025));
    // this.hexs.push(new Hexagon(1, false, 0x877CFF, 0, -1.2, 2, 578, 234, 0.04));
    // this.hexs.push(new Hexagon(2, false, 0x249ACC, 0, 2.7, 2.3, 12, -23, 0.01));
    // this.hexs.push(new Hexagon(3, false, 0x88BFFF, 0, -1, -2, 0, 0, 0.03));
    this.generateHexagons();
    //todo(eh): create a bunch of hexagons for the screen of differnt types
    // this.scene.add(this.hex);
    for (const hex of this.hexs) {
      hex.addToScene(this.scene);
    }

    let aspectRatio = this.getAspectRatio();
    this.camera = new THREE.OrthographicCamera(
      this.canvas.clientWidth / - 2, this.canvas.clientWidth / 2, this.canvas.clientHeight / 2, this.canvas.clientHeight / - 2, 1 , 1000
    );
    this.camera.position.z = 1;
  }

  private startRenderingLoop() {
    this.renderer = new THREE.WebGLRenderer({ canvas: this.canvas, alpha: true, antialias: true });
    this.renderer.setPixelRatio(devicePixelRatio);
    this.renderer.setSize(this.canvas.clientWidth, this.canvas.clientHeight);

    let component: WallpaperComponent = this;
    (function render() {
      requestAnimationFrame(render);
      component.animate();
      component.renderer.render(component.scene, component.camera);
    }());
  }

  private getAspectRatio() {
    return this.canvas.clientWidth / this.canvas.clientHeight;
  }

  private animate() {
    for (const hex of this.hexs) {
      hex.animate(this.canvas);
    }
  }

  private generateHexagons() {

    let amount = this.canvas.clientWidth / 100;

    for (let i = 0; i < amount; i++) {

      let hex = new Hexagon(
        this.getRandomFloat(0.2, 3, 1),
        Math.random() < 0.5, this.hexColours[Math.floor(Math.random() * this.hexColours.length)],
        0,
        this.getRandomFloat(-2, 2, 1), this.getRandomFloat(-2, 2, 1),
        this.getRandomFloat(this.canvas.clientWidth / - 2, this.canvas.clientWidth / 2, 1),
        this.getRandomFloat(this.canvas.clientHeight / - 2, this.canvas.clientHeight / 2, 1),
        this.getRandomFloat(-0.01, 0.01, 3));

      this.hexs.push(hex);
    }
  }

  private getRandomFloat(min: number, max: number, decimals: number) {
    const str = (Math.random() * (max - min) + min).toFixed(decimals);

    return parseFloat(str);
  }


}
